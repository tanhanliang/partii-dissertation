\contentsline {chapter}{\numberline {1}Introduction}{5}
\contentsline {section}{\numberline {1.1}The Problem}{5}
\contentsline {section}{\numberline {1.2}My Project}{6}
\contentsline {subsection}{\numberline {1.2.1}Related Work}{6}
\contentsline {section}{\numberline {1.3}A Promising Approach: Patchy-San}{7}
\contentsline {section}{\numberline {1.4}Work Done}{7}
\contentsline {section}{\numberline {1.5}Licence}{7}
\contentsline {chapter}{\numberline {2}Preparation}{8}
\contentsline {section}{\numberline {2.1}Requirements Analysis}{8}
\contentsline {section}{\numberline {2.2}Starting Point}{9}
\contentsline {section}{\numberline {2.3}Background Theory}{9}
\contentsline {subsection}{\numberline {2.3.1}Patchy-San}{9}
\contentsline {subsection}{\numberline {2.3.2}Perceptrons}{10}
\contentsline {subsection}{\numberline {2.3.3}Multilayer Perceptrons}{11}
\contentsline {subsection}{\numberline {2.3.4}Convolutional Neural Networks}{12}
\contentsline {subsection}{\numberline {2.3.5}Gradient Descent and Backpropagation}{13}
\contentsline {subsection}{\numberline {2.3.6}Other Network Layers}{14}
\contentsline {subsection}{\numberline {2.3.7}Stochastic Optimisation Using Adam}{15}
\contentsline {subsection}{\numberline {2.3.8}Activation Functions}{15}
\contentsline {section}{\numberline {2.4}Setting Up}{16}
\contentsline {chapter}{\numberline {3}Implementation}{17}
\contentsline {section}{\numberline {3.1}Characteristics of the Data Provided}{17}
\contentsline {subsection}{\numberline {3.1.1}Node and Edge Properties}{17}
\contentsline {subsection}{\numberline {3.1.2}Sparse Graph Structure}{18}
\contentsline {subsection}{\numberline {3.1.3}Graph Community Structure}{19}
\contentsline {subsection}{\numberline {3.1.4}Variable Length Graph Patterns}{20}
\contentsline {subsection}{\numberline {3.1.5}Lack of Patterns Describing Malicious Behavior}{21}
\contentsline {section}{\numberline {3.2}Graph Representation}{21}
\contentsline {section}{\numberline {3.3}Preprocessing Steps}{22}
\contentsline {subsection}{\numberline {3.3.1}Removal of Anomalous Nodes}{22}
\contentsline {subsection}{\numberline {3.3.2}Consolidation of Node Versions}{22}
\contentsline {subsection}{\numberline {3.3.3}Renaming Symlinked files}{23}
\contentsline {section}{\numberline {3.4}Overview of Model Pipeline}{23}
\contentsline {section}{\numberline {3.5}Patchy-San}{24}
\contentsline {subsection}{\numberline {3.5.1}Node Sequence Selection}{24}
\contentsline {subsection}{\numberline {3.5.2}Neighbourhood Assembly}{25}
\contentsline {subsection}{\numberline {3.5.3}Receptive Field Normalisation}{25}
\contentsline {subsection}{\numberline {3.5.4}Output from Patchy-San}{26}
\contentsline {section}{\numberline {3.6}Construction of Input Tensors}{27}
\contentsline {subsection}{\numberline {3.6.1}Input Tensor for Node Properties}{28}
\contentsline {subsection}{\numberline {3.6.2}Input Tensor for Edge Properties}{28}
\contentsline {subsection}{\numberline {3.6.3}Input Tensor for Word Embeddings}{28}
\contentsline {section}{\numberline {3.7}Application of a Convolutional Neural Network}{30}
\contentsline {section}{\numberline {3.8}Training Data Generation}{31}
\contentsline {chapter}{\numberline {4}Evaluation}{32}
\contentsline {section}{\numberline {4.1}Evaluation Metrics}{32}
\contentsline {subsection}{\numberline {4.1.1}Precision and Recall}{32}
\contentsline {subsection}{\numberline {4.1.2}Accuracy}{33}
\contentsline {section}{\numberline {4.2}Performance with Different Pattern Complexities}{34}
\contentsline {section}{\numberline {4.3}Model and Pipeline Performance with Different Pattern Sizes}{40}
\contentsline {chapter}{\numberline {5}Conclusion}{42}
\contentsline {section}{\numberline {5.1}Work Done}{42}
\contentsline {section}{\numberline {5.2}Future Work}{42}
